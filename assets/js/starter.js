// Global vers
var periodicFunctions = {};

/* Loaders (both interface and logic) */
function loadBoard(type){
	var map;
	switch(type){
		case "eur":
			map = {
				dimensions : {w: 9, h: 9},
				0 : {0 : null, 1 : null, 2 : null, 3 : null, 4 : null, 5 : null, 6 : null, 7 : null, 8 : null},
				1 : {0 : null, 1 : null, 2 : null, 3 : false, 4 : false, 5 : false, 6 : null, 7 : null, 8 : null},
				2 : {0 : null, 1 : null, 2 : false, 3 : false, 4 : false, 5 : false, 6 : false, 7 : null, 8 : null},
				3 : {0 : null, 1 : false, 2 : false, 3 : false, 4 : false, 5 : false, 6 : false, 7 : false, 8 : null},
				4 : {0 : null, 1 : false, 2 : false, 3 : false, 4 : false, 5 : false, 6 : false, 7 : false, 8 : null},
				5 : {0 : null, 1 : false, 2 : false, 3 : false, 4 : false, 5 : false, 6 : false, 7 : false, 8 : null},
				6 : {0 : null, 1 : null, 2 : false, 3 : false, 4 : false, 5 : false, 6 : false, 7 : null, 8 : null},
				7 : {0 : null, 1 : null, 2 : null, 3 : false, 4 : false, 5 : false, 6 : null, 7 : null, 8 : null},
				8 : {0 : null, 1 : null, 2 : null, 3 : null, 4 : null, 5 : null, 6 : null, 7 : null, 8 : null}
			};
			break;
		case "wie":
			map = {
				dimensions : {w: 9, h: 9},
				0 : {0 : null, 1 : null, 2 : null, 3 : false, 4 : false, 5 : false, 6 : null, 7 : null, 8 : null},
				1 : {0 : null, 1 : null, 2 : null, 3 : false, 4 : false, 5 : false, 6 : null, 7 : null, 8 : null},
				2 : {0 : null, 1 : null, 2 : null, 3 : false, 4 : false, 5 : false, 6 : null, 7 : null, 8 : null},
				3 : {0 : false, 1 : false, 2 : false, 3 : false, 4 : false, 5 : false, 6 : false, 7 : false, 8 : false},
				4 : {0 : false, 1 : false, 2 : false, 3 : false, 4 : false, 5 : false, 6 : false, 7 : false, 8 : false},
				5 : {0 : false, 1 : false, 2 : false, 3 : false, 4 : false, 5 : false, 6 : false, 7 : false, 8 : false},
				6 : {0 : null, 1 : null, 2 : null, 3 : false, 4 : false, 5 : false, 6 : null, 7 : null, 8 : null},
				7 : {0 : null, 1 : null, 2 : null, 3 : false, 4 : false, 5 : false, 6 : null, 7 : null, 8 : null},
				8 : {0 : null, 1 : null, 2 : null, 3 : false, 4 : false, 5 : false, 6 : null, 7 : null, 8 : null}
			};
			break;
		case "asy":
			map = {
				dimensions : {w: 9, h: 9},
				0 : {0 : null, 1 : null, 2 : null, 3 : false, 4 : false, 5 : false, 6 : null, 7 : null, 8 : null},
				1 : {0 : null, 1 : null, 2 : null, 3 : false, 4 : false, 5 : false, 6 : null, 7 : null, 8 : null},
				2 : {0 : null, 1 : null, 2 : null, 3 : false, 4 : false, 5 : false, 6 : null, 7 : null, 8 : null},
				3 : {0 : null, 1 : false, 2 : false, 3 : false, 4 : false, 5 : false, 6 : false, 7 : false, 8 : false},
				4 : {0 : null, 1 : false, 2 : false, 3 : false, 4 : false, 5 : false, 6 : false, 7 : false, 8 : false},
				5 : {0 : null, 1 : false, 2 : false, 3 : false, 4 : false, 5 : false, 6 : false, 7 : false, 8 : false},
				6 : {0 : null, 1 : null, 2 : null, 3 : false, 4 : false, 5 : false, 6 : null, 7 : null, 8 : null},
				7 : {0 : null, 1 : null, 2 : null, 3 : false, 4 : false, 5 : false, 6 : null, 7 : null, 8 : null},
				8 : {0 : null, 1 : null, 2 : null, 3 : null, 4 : null, 5 : null, 6 : null, 7 : null, 8 : null}
			};
			break;
		case "dia":
			map = {
				dimensions : {w: 9, h: 9},
				0 : {0 : null, 1 : null, 2 : null, 3 : null, 4 : false, 5 : null, 6 : null, 7 : null, 8 : null},
				1 : {0 : null, 1 : null, 2 : null, 3 : false, 4 : false, 5 : false, 6 : null, 7 : null, 8 : null},
				2 : {0 : null, 1 : null, 2 : false, 3 : false, 4 : false, 5 : false, 6 : false, 7 : null, 8 : null},
				3 : {0 : null, 1 : false, 2 : false, 3 : false, 4 : false, 5 : false, 6 : false, 7 : false, 8 : null},
				4 : {0 : false, 1 : false, 2 : false, 3 : false, 4 : false, 5 : false, 6 : false, 7 : false, 8 : false},
				5 : {0 : null, 1 : false, 2 : false, 3 : false, 4 : false, 5 : false, 6 : false, 7 : false, 8 : null},
				6 : {0 : null, 1 : null, 2 : false, 3 : false, 4 : false, 5 : false, 6 : false, 7 : null, 8 : null},
				7 : {0 : null, 1 : null, 2 : null, 3 : false, 4 : false, 5 : false, 6 : null, 7 : null, 8 : null},
				8 : {0 : null, 1 : null, 2 : null, 3 : null, 4 : false, 5 : null, 6 : null, 7 : null, 8 : null}
			};
			break;
		default:
			map = {
				dimensions : {w: 9, h: 9},
				0 : {0 : null, 1 : null, 2 : null, 3 : null, 4 : null, 5 : null, 6 : null, 7 : null, 8 : null},
				1 : {0 : null, 1 : null, 2 : null, 3 : false, 4 : false, 5 : false, 6 : null, 7 : null, 8 : null},
				2 : {0 : null, 1 : null, 2 : null, 3 : false, 4 : false, 5 : false, 6 : null, 7 : null, 8 : null},
				3 : {0 : null, 1 : false, 2 : false, 3 : false, 4 : false, 5 : false, 6 : false, 7 : false, 8 : null},
				4 : {0 : null, 1 : false, 2 : false, 3 : false, 4 : false, 5 : false, 6 : false, 7 : false, 8 : null},
				5 : {0 : null, 1 : false, 2 : false, 3 : false, 4 : false, 5 : false, 6 : false, 7 : false, 8 : null},
				6 : {0 : null, 1 : null, 2 : null, 3 : false, 4 : false, 5 : false, 6 : null, 7 : null, 8 : null},
				7 : {0 : null, 1 : null, 2 : null, 3 : false, 4 : false, 5 : false, 6 : null, 7 : null, 8 : null},
				8 : {0 : null, 1 : null, 2 : null, 3 : null, 4 : null, 5 : null, 6 : null, 7 : null, 8 : null}
			};
			break;
	}
	loadVisualBoard(map);
	setLogicBoard(map);
}

function loadRandomBoard(){
	var boards = ["eur", "wie", "asy", "dia", "eng"];
	var num =  Math.floor(Math.random() * 5);
	loadBoard(boards[num]);
}

function loadNewGame(){
	// Stop presentation screen.
	clearInterval(periodicFunctions.welcomeBoard);
	gameStarted = true;
	// Load form data.
	var form = document.querySelector('#form-new-game').children[0];
	var gameParam = {};
	gameParam.board = getInputRadioData(form, 'board');
	gameParam.gameDuration = getInputData(form, 'gameDuration');
	gameParam.holePosition = getInputRadioData(form, 'holePosition');
	loadBoard(gameParam.board);
	setHole(gameParam.holePosition);
	setPointCounter(0);
	if(gameParam.gameDuration > 0){
		// Set timer
		setTimer(gameParam.gameDuration);
		periodicFunctions.gameTimer = setInterval(countDownTimer, 1000);
	}
}

function endGame(){
	// Disable cells.
	var cells = getBoardCells();
	for(i=0; i<cells.length; i++){
		cells[i].removeEventListener('click', eventClickPosition);
	}
	var orbs = getOrbs();
	if(orbs.length === 1 && (orbs[0].h === 4 && orbs[0].w === 4)){
		// Add 150 points if exists unique orb in center.
		setPointCounter(points + 150);
	} else {
		// Remove 50 points each additional orb.
		setPointCounter(points - (50 * (orbs.length -1)));
	}
	// Open win dialog.
	eventSection(null, {id:'winner'});
}

function resetApp(){
	// Reset game variables.
	gameStarted = false;
	clickedCell = undefined;
	user = "user";
	timer = undefined;
	points = undefined;
	logicBoard = undefined;
	visualBoard = undefined;

	// Start welcome screen.
	loadRandomBoard();
	loadLeaderBoard();
	setMessage("Welcome "+user);
	setPointCounter("---");
	setTimer("---");
	periodicFunctions.welcomeBoard = setInterval(randomPointsOnBoard, 50);
}

function startApp(){
	loadListeners();
	resetApp();
}


// Start the app.
startApp();