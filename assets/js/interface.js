// Global vars
var visualBoard;

// Dialog managers
function actionOpenDialog(formId){
	document.querySelector(formId).className = 'fade-in';
}

function actionCloseDialog(formId){
	document.querySelector(formId).className = 'fade-out';
}

// Section managers
function actionOpenLeft(formId){
	document.querySelector(formId).className = 'current';
	document.querySelector('[data-position="current"]').className = 'left';
}

function actionCloseLeft(formId){
	document.querySelector(formId).className = 'right';
	document.querySelector('[data-position="current"]').className = 'current';
}

function actionOpenRight(formId){
	document.querySelector(formId).className = 'current';
	document.querySelector('[data-position="current"]').className = 'right';
}

function actionCloseRight(formId){
	document.querySelector(formId).className = 'left';
	document.querySelector('[data-position="current"]').className = 'current';
}

// Action checker
function actionCheck(elementId, sectionId, dialog, to){
	if(elementId.indexOf('exit') > -1){
		if(dialog){
			actionCloseDialog('#'+sectionId);
		} else if(to === 'left'){
			actionCloseLeft('#'+sectionId);
		} else {
			actionCloseRight('#'+sectionId);
		}
		if(!gameStarted){
			periodicFunctions.welcomeBoard = setInterval(randomPointsOnBoard, 50);
		}
	} else {
		clearInterval(periodicFunctions.welcomeBoard);
		if(dialog){
			actionOpenDialog('#'+sectionId);
		} else if(to === 'left'){
			actionOpenLeft('#'+sectionId);
		} else {
			actionOpenRight('#'+sectionId);
		}
	}
}

// Interface events
function eventSection(event, form){
	var dialog;
	var element = form || this;
	if(element.id.indexOf('about') > -1){
		dialog = true;
		actionCheck(element.id, 'about', dialog);
	}
	if(element.id.indexOf('new-game') > -1){
		dialog = false;
		actionCheck(element.id, 'new-game', dialog, 'left');
	}
	if(element.id.indexOf('start-game') > -1){
		event.preventDefault();
		dialog = false;
		actionCheck('exit', 'new-game', dialog, 'left');
		loadNewGame();
	}
	if(element.id.indexOf('how-to') > -1){
		dialog = false;
		actionCheck(element.id, 'how-to', dialog, 'left');
	}
	if(element.id.indexOf('leaderboard') > -1){
		dialog = false;
		actionCheck(element.id, 'leaderboard', dialog, 'left');
		if(element.id.indexOf('clear') > -1){
			leaderBoard = undefined;
			loadLeaderBoard();
		}
		if(element.id.indexOf('exit') === -1){
			loadLeaderBoard();
		}
	}
	if(element.id.indexOf('winner') > -1){
		dialog = true;
		actionCheck(element.id, 'winner', dialog);
		if(element.id.indexOf('exit') === -1){
			syncGameScore();
		} else {
			saveGameScore();
			resetApp();
		}
	}
	return false;
}

function eventFormSection(event){
	eventSection(event, {id:'start-game'});
}

function eventClickPosition(event){
	var pos = this.id.substring(5,7).split('');
	var orbMoved = false;
	if(clickedCell !== undefined){
		// 1º If group: Check if dest is ok.
		if(parseInt(clickedCell[0])+2 == pos[0] && parseInt(clickedCell[1]) == pos[1]){
			// 2º If group: Check if dest is a hole.
			if(logicBoard[pos[0]][pos[1]] === false){
				// 3º If group: Check if exists orb in pos[0]-1
				if(logicBoard[parseInt(pos[0])-1][parseInt(pos[1])] === true){
					// Change values.
					logicBoard[pos[0]][pos[1]] = true;
					logicBoard[clickedCell[0]][clickedCell[1]] = false;
					logicBoard[parseInt(pos[0])-1][parseInt(pos[1])] = false;
					orbMoved = true;
				}
			}
		}
		if(parseInt(clickedCell[0]) == pos[0] && parseInt(clickedCell[1])+2 == pos[1]){
			if(logicBoard[pos[0]][pos[1]] === false){
				if(logicBoard[parseInt(pos[0])][parseInt(pos[1])-1] === true){
					logicBoard[pos[0]][pos[1]] = true;
					logicBoard[clickedCell[0]][clickedCell[1]] = false;
					logicBoard[parseInt(pos[0])][parseInt(pos[1])-1] = false;
					orbMoved = true;
				}
			}
		}
		if(parseInt(clickedCell[0])-2 == pos[0] && parseInt(clickedCell[1]) == pos[1]){
			if(logicBoard[pos[0]][pos[1]] === false){
				if(logicBoard[parseInt(pos[0])+1][parseInt(pos[1])] === true){
					logicBoard[pos[0]][pos[1]] = true;
					logicBoard[clickedCell[0]][clickedCell[1]] = false;
					logicBoard[parseInt(pos[0])+1][parseInt(pos[1])] = false;
					orbMoved = true;
				}
			}
		}
		if(parseInt(clickedCell[0]) == pos[0] && parseInt(clickedCell[1])-2 == pos[1]){
			if(logicBoard[pos[0]][pos[1]] === false){
				if(logicBoard[parseInt(pos[0])][parseInt(pos[1])+1] === true){
					logicBoard[pos[0]][pos[1]] = true;
					logicBoard[clickedCell[0]][clickedCell[1]] = false;
					logicBoard[parseInt(pos[0])][parseInt(pos[1])+1] = false;
					orbMoved = true;
				}
			}
		}
		if(orbMoved){
			setPointCounter(points+15);
			var canMove = haveMoves();
			if(!canMove){
				endGame();
			}
		}
		clickedCell = undefined;
		syncVisualBoard();
	} else {
		if(logicBoard[pos[0]][pos[1]]){
			this.children[0].className = this.children[0].className+' active';
			clickedCell = pos;
		}
	}
}

// Visual sync
function syncVisualBoard(){
	var cell;
	visualBoard = logicBoard;
	for(i=0; i<visualBoard.dimensions.h; i++){
		for(j=0; j<visualBoard.dimensions.w; j++){
			cell = document.querySelector('#cell-'+i+j);
			if(visualBoard[i][j]!==null){
				if(visualBoard[i][j]){
					cell.children[0].className = 'circle orb';
				} else if(!visualBoard[i][j]){
					cell.children[0].className = 'circle hole';
				}
			}
		}
	}
}

function syncLeaderBoard(){
	var leaderTable = document.querySelector('#leaders-table');
	var leaderTableHtml = "";
	for(i=0; i<leaderBoard.dimensions; i++){
		leaderTableHtml += '<tr><td>'+leaderBoard[i].user+'</td><td>'+leaderBoard[i].points+'</td></tr>';
	}
	leaderTable.children[1].innerHTML = leaderTableHtml;
}

function syncTimer(){
	document.querySelector('#seconds').innerHTML = timer;
}

function syncPoints(){
	var counter = document.querySelector('#points');
	counter.innerHTML = points;
	if(points >= 0){
		counter.className = 'positive';
	} else {
		counter.className = 'negative';
	}
}

function syncGameScore(){
	var counter = document.querySelector('#final-points');
	counter.innerHTML = points;
	if(points >= 0){
		counter.className = 'positive';
	} else {
		counter.className = 'negative';
	}
	document.querySelector('#final-time').innerHTML = timer;
}

// Setters 
function setMessage(message){
	document.querySelector('#messages').innerHTML = '<span>'+message+'</span>';
}

// Getters
function getBoardCells(){
	var elems = [];
	var divs = document.getElementsByTagName("div");
	for(i = 0; i < divs.length; i++) {
    	if(divs[i].id.indexOf('cell-') === 0) {
        	elems.push(divs[i]);
    	}
	}
	return elems;
}

// Loaders
function loadVisualBoard(map){
	visualBoard = map;
	var board = document.querySelector('.board-inner');
	var row, col, orbhole;
	board.innerHTML = '';
	for(i=0; i<visualBoard.dimensions.h; i++){
		row = document.createElement("div");
		row.className = 'row';
		for(j=0; j<visualBoard.dimensions.w; j++){
			col = document.createElement("div");
			col.className = 'col';
			col.id = 'cell-'+i+j;
			orbhole = document.createElement("div");
			if(visualBoard[i][j] !== null){
				col.addEventListener('click', eventClickPosition);
				if(visualBoard[i][j]){
					orbhole.className = 'circle orb';
				} else if(!visualBoard[i][j]){
					orbhole.className = 'circle hole';
				}
			}
			col.appendChild(orbhole);
			row.appendChild(col);
		}
		board.appendChild(row);
	}

}

function loadListeners(){
	// Listeners
	document.querySelector('#nav-about').addEventListener('click', eventSection);
	document.querySelector('#btn-exit-about').addEventListener('click', eventSection);
	document.querySelector('#nav-new-game').addEventListener('click', eventSection);
	document.querySelector('#btn-new-game').addEventListener('click', eventSection);
	document.querySelector('#nav-exit-new-game').addEventListener('click', eventSection);
	document.querySelector('#btn-exit-new-game').addEventListener('click', eventSection);
	document.querySelector('#nav-start-game').addEventListener('click', eventSection);
	document.querySelector('#btn-start-game').addEventListener('click', eventSection);
	document.querySelector('#nav-how-to').addEventListener('click', eventSection);
	document.querySelector('#nav-exit-how-to').addEventListener('click', eventSection);
	document.querySelector('#nav-leaderboard').addEventListener('click', eventSection);
	document.querySelector('#nav-exit-leaderboard').addEventListener('click', eventSection);
	document.querySelector('#btn-exit-winner').addEventListener('click', eventSection);
	document.querySelector('#btn-clear-leaderboard').addEventListener('click', eventSection);
}