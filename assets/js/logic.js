// Global vars
var gameStarted;
var clickedCell;
var user;
var timer;
var points;
var logicBoard;
var leaderBoard;

// Checkers
function checkLocalStorage() {
  try {
    return 'localStorage' in window && window.localStorage !== null;
  } catch (e) {
    return false;
  }
}

function haveMoves(){
	// Search all orbs
	for(i=0; i<logicBoard.dimensions.h; i++){
		for(j=0; j<logicBoard.dimensions.w; j++){
			// Is position out of board?
			if(logicBoard[i][j] !== undefined && logicBoard[i][j] !== null){
				// Is position true?
				if(logicBoard[i][j]){
					// It's orb.
					// Hole exists around this orb?
					if(logicBoard[i-1] !== undefined &&
						logicBoard[i-1] !== null &&
						logicBoard[i-1][j] !== undefined &&
						logicBoard[i-1][j] !== null){

						if(logicBoard[i-1][j]){
							// Exists one orb around.
							// Hole exists next position to existing orb?
							if(logicBoard[i-2] !== undefined &&
								logicBoard[i-2] !== null &&
								logicBoard[i-2][j] !== undefined &&
								logicBoard[i-2][j] !== null){

								if(!logicBoard[i-2][j]){
									return true;
								}

							}

						}

					}
					if(logicBoard[i][j+1] !== undefined &&
						logicBoard[i][j+1] !== null){

						if(logicBoard[i][j+1]){

							if(logicBoard[i][j+2] !== undefined &&
								logicBoard[i][j+2] !== null){

								if(!logicBoard[i][j+2]){
									return true;
								}

							}

						}

					}
					if(logicBoard[i+1] !== undefined &&
						logicBoard[i+1] !== null &&
						logicBoard[i+1][j] !== undefined &&
						logicBoard[i+1][j] !== null){

						if(logicBoard[i+1][j]){

							if(logicBoard[i+2] !== undefined &&
								logicBoard[i+2] !== null &&
								logicBoard[i+2][j] !== undefined &&
								logicBoard[i+2][j] !== null){

								if(!logicBoard[i+2][j]){
									return true;
								}

							}

						}

					}
					if(logicBoard[i][j-1] !== undefined &&
						logicBoard[i][j-1] !== null){

						if(logicBoard[i][j-1]){

							if(logicBoard[i][j-2] !== undefined &&
								logicBoard[i][j-2] !== null){

								if(!logicBoard[i][j-2]){
									return true;
								}

							}

						}

					}
				}
			}
		}
	}
	return false;
}

// Randomizers
function randomPointsOnBoard(){
	var i = Math.floor(Math.random()*9);
	var j = Math.floor(Math.random()*9);
	if(logicBoard[i][j] !== null){
		if(logicBoard[i][j]){
			logicBoard[i][j] = false;
		} else {
			logicBoard[i][j] = true;
		}
	}
	syncVisualBoard();
}

// Setters
function setLogicBoard(map){
	logicBoard = map;
}

function setTimer(time){
	timer = time;
	syncTimer();
}

function setPointCounter(point){
	points = point;
	syncPoints();
}

function setHole(option){
	var holeSet = false;
	for(i=0; i<logicBoard.dimensions.h; i++){
		for(j=0; j<logicBoard.dimensions.w; j++){
			if(logicBoard[i][j] !== null){
				logicBoard[i][j] = true;
			}
		}
	}
	if(option === "central"){
		logicBoard[4][4] = false;
		holeSet = true;
	} else if(option === "random"){
		while(!holeSet){
			var i = Math.floor(Math.random()*9);
			var j = Math.floor(Math.random()*9);
			if(logicBoard[i][j] !== null){
				if(logicBoard[i][j]){
					logicBoard[i][j] = false;
					holeSet = true;
				}
			}
		}
	}
	syncVisualBoard();
}

// Countdown
function countDownTimer(){
	if(timer <= 0){
		// Stop timer
		clearInterval(periodicFunctions.gameTimer);
		// Disable board and set points.
		endGame();
	} else {
		setTimer(--timer);
	}
}

// Loaders
function loadDefaultLeaderBoard(){
	leaderBoard = {
		dimensions: 10,
		0:{user:"Aglezabad", points: 630},
		1:{user:"Aglezabad", points: 580},
		2:{user:"Aglezabad", points: 530},
		3:{user:"Aglezabad", points: 480},
		4:{user:"Aglezabad", points: 430},
		5:{user:"Aglezabad", points: 380},
		6:{user:"Aglezabad", points: 330},
		7:{user:"Aglezabad", points: 280},
		8:{user:"Aglezabad", points: 230},
		9:{user:"Aglezabad", points: 180}
	};
}

function loadLeaderBoard(){
	if(checkLocalStorage()){
		if(leaderBoard === undefined){
			leaderBoard = JSON.parse(localStorage.getItem("pegSolitaireRanking"));
		}
		if(leaderBoard === null){
			// No data on local storage. Load default data.
			loadDefaultLeaderBoard();
		}
	} else {
		alert("Can't load from local storage because is not available. Your ranking will be lost.");
	}
	syncLeaderBoard();
}

// Savers
function saveLeaderBoard(){
	if(checkLocalStorage()){
		localStorage.setItem("pegSolitaireRanking", JSON.stringify(leaderBoard));
	} else {
		alert("Can't save on local storage because is not available. Your ranking will be lost.");
	}
	syncLeaderBoard();
}

function saveGameScore(){
	var i=0, set=false;
	while(i<leaderBoard.dimensions && !set){
		if(leaderBoard[i].points<points){
			leaderBoard[i].user = user;
			leaderBoard[i].points = points;
			set=true;
		} else {
			i++;
		}
	}
	saveLeaderBoard();
}

// Getters
function getInputRadioData(form, name){
	var inputData;
	var radios = form.elements[name];
	for(i=0; i<radios.length; i++){
		if(radios[i].checked){
			inputData = radios[i].value;
		}
	}
	return inputData;
}

function getInputData(form, name){
	var input = form.elements[name];
	return input.value;
}

function getOrbs(){
	var orbs = [];
	for(i=0; i<logicBoard.dimensions.h; i++){
		for(j=0; j<logicBoard.dimensions.w; j++){
			if(logicBoard[i][j]){
				orbs.push({h: i, w: j});
			}
		}
	}
	return orbs;
}